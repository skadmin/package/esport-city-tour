<?php

declare(strict_types=1);

namespace Skadmin\EsportCityTour\Doctrine\EsportCityTour;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\EsportCityTour\Doctrine\EsportCityTourType\EsportCityTourType;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class EctCity
{
    use Entity\BaseEntity;
    use Entity\Name;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;

    #[ORM\Column(nullable: true)]
    private ?string $imagePreviewCity = '';

    #[ORM\Column(nullable: true)]
    private ?string $imagePreviewParticipant = '';

    public function update(string $name, bool $isActive, ?string $imagePreview, ?string $imagePreviewCity, ?string $imagePreviewParticipant): void
    {
        $this->name = $name;
        $this->setIsActive($isActive);

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }

        if ($imagePreviewCity !== null && $imagePreviewCity !== '') {
            $this->imagePreviewCity = $imagePreviewCity;
        }

        if ($imagePreviewParticipant !== null && $imagePreviewParticipant !== '') {
            $this->imagePreviewParticipant = $imagePreviewParticipant;
        }
    }

    public function getImagePreviewCity(): ?string
    {
        if ($this->imagePreviewCity !== null) {
            return $this->imagePreviewCity === '' ? null : $this->imagePreviewCity;
        }

        return $this->imagePreviewCity;
    }

    public function getImagePreviewParticipant(): ?string
    {
        if ($this->imagePreviewParticipant !== null) {
            return $this->imagePreviewParticipant === '' ? null : $this->imagePreviewParticipant;
        }

        return $this->imagePreviewParticipant;
    }

}
