<?php

declare(strict_types=1);

namespace Skadmin\EsportCityTour\Doctrine\EsportCityTour;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits;
use SkadminUtils\DoctrineTraits\Facade;

final class EctCityFacade extends Facade
{
    use DoctrineTraits\Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = EctCity::class;
    }

    public function create(string $name, bool $isActive, ?string $imagePreview, ?string $imagePreviewCity, ?string $imagePreviewParticipant): EctCity
    {
        return $this->update(null, $name, $isActive, $imagePreview, $imagePreviewCity, $imagePreviewParticipant);
    }

    public function update(?int $id, string $name, bool $isActive, ?string $imagePreview, ?string $imagePreviewCity, ?string $imagePreviewParticipant): EctCity
    {
        $ectCity = $this->get($id);
        $ectCity->update($name, $isActive, $imagePreview, $imagePreviewCity, $imagePreviewParticipant);

        if (! $ectCity->isLoaded()) {
            $ectCity->setSequence($this->getValidSequence());
        }

        $this->em->persist($ectCity);
        $this->em->flush();

        return $ectCity;
    }

    public function get(?int $id = null): EctCity
    {
        if ($id === null) {
            return new EctCity();
        }

        $esportCityTour = parent::get($id);

        if ($esportCityTour === null) {
            return new EctCity();
        }

        return $esportCityTour;
    }

    /**
     * @return EctCity[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

}
