<?php

declare(strict_types=1);

namespace Skadmin\EsportCityTour;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE              = 'esport-city-tour';
    public const DIR_IMAGE             = 'esport-city-tour';
    public const DIR_IMAGE_CITY        = 'esport-city-tour-city';
    public const DIR_IMAGE_PARTICIPANT = 'esport-city-tour-participant';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-city']),
            'items'   => ['overview-ect-city'],
        ]);
    }
}
