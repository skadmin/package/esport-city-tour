<?php

declare(strict_types=1);

namespace Skadmin\EsportCityTour\Components\Admin;

interface IEditEctCityFactory
{
    public function create(?int $id = null): EditEctCity;
}
