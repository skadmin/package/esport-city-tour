<?php

declare(strict_types=1);

namespace Skadmin\EsportCityTour\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\EsportCityTour\Doctrine\EsportCityTour\EctCity;
use Skadmin\EsportCityTour\Doctrine\EsportCityTour\EctCityFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\EsportCityTour\BaseControl;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditEctCity extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private EctCityFacade $facade;
    private EctCity       $ectCity;
    private ImageStorage  $imageStorage;

    public function __construct(?int $id, EctCityFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->ectCity = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->ectCity->isLoaded()) {
            return new SimpleTranslation('esport-city-tour.edit-ect-city.title - %s', $this->ectCity->getName());
        }

        return 'esport-city-tour.edit-ect-city.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editEctCity.latte');

        $template->ectCity = $this->ectCity;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.esport-city-tour.edit-ect-city.name')
            ->setRequired('form.esport-city-tour.edit-ect-city.name.req');
        $form->addCheckbox('isActive', 'form.esport-city-tour.edit-ect-city.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('imagePreview', 'form.esport-city-tour.edit-ect-city.image-preview');
        $form->addImageWithRFM('imagePreviewCity', 'form.esport-city-tour.edit-ect-city.image-preview-city');
        $form->addImageWithRFM('imagePreviewParticipant', 'form.esport-city-tour.edit-ect-city.image-preview-participant');

        // BUTTON
        $form->addSubmit('send', 'form.esport-city-tour.edit-ect-city.send');
        $form->addSubmit('sendBack', 'form.esport-city-tour.edit-ect-city.send-back');
        $form->addSubmit('back', 'form.esport-city-tour.edit-ect-city.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier            = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);
        $identifierCity        = UtilsFormControl::getImagePreview($values->imagePreviewCity, BaseControl::DIR_IMAGE_CITY);
        $identifierParticipant = UtilsFormControl::getImagePreview($values->imagePreviewParticipant, BaseControl::DIR_IMAGE_PARTICIPANT);

        if ($this->ectCity->isLoaded()) {
            if ($identifier !== null && $this->ectCity->getImagePreview() !== null) {
                $this->imageStorage->delete($this->ectCity->getImagePreview());
            }
            if ($identifierCity !== null && $this->ectCity->getImagePreviewCity() !== null) {
                $this->imageStorage->delete($this->ectCity->getImagePreviewCity());
            }
            if ($identifierParticipant !== null && $this->ectCity->getImagePreviewParticipant() !== null) {
                $this->imageStorage->delete($this->ectCity->getImagePreviewParticipant());
            }

            $ectCity = $this->facade->update(
                $this->ectCity->getId(),
                $values->name,
                $values->isActive,
                $identifier,
                $identifierCity,
                $identifierParticipant
            );
            $this->onFlashmessage('form.esport-city-tour.edit-ect-city.flash.success.update', Flash::SUCCESS);
        } else {
            $ectCity = $this->facade->create(
                $values->name,
                $values->isActive,
                $identifier,
                $identifierCity,
                $identifierParticipant
            );
            $this->onFlashmessage('form.esport-city-tour.edit-ect-city.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-ect-city',
            'id'      => $ectCity->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-ect-city',
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->ectCity->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->ectCity->getName(),
            'isActive' => $this->ectCity->isActive(),
        ];
    }
}
