<?php

declare(strict_types=1);

namespace Skadmin\EsportCityTour\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\EsportCityTour\Doctrine\EsportCityTour\EctCity;
use Skadmin\EsportCityTour\Doctrine\EsportCityTour\EctCityFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\EsportCityTour\BaseControl;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class OverviewEctCity extends GridControl
{
    use APackageControl;
    use IsActive;

    private EctCityFacade $facade;
    private LoaderFactory $webLoader;
    private ImageStorage  $imageStorage;

    public function __construct(EctCityFacade $facade, Translator $translator, User $user, ImageStorage $imageStorage, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewEctCity.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'esport-city-tour.overview-ect-city.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.sequence'));

        // COLUMNS
        $grid->addColumnText('imagePreview', 'grid.esport-city-tour.overview-ect-city.image-preview')
            ->setRenderer(function (EctCity $ectCity): ?Html {
                if ($ectCity->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$ectCity->getImagePreview(), '120x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('imagePreviewCity', 'grid.esport-city-tour.overview-ect-city.image-preview-city')
            ->setRenderer(function (EctCity $ectCity): ?Html {
                if ($ectCity->getImagePreviewCity() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$ectCity->getImagePreviewCity(), '60x60', 'fit']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('imagePreviewParticipant', 'grid.esport-city-tour.overview-ect-city.image-preview-participant')
            ->setRenderer(function (EctCity $ectCity): ?Html {
                if ($ectCity->getImagePreviewParticipant() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$ectCity->getImagePreviewParticipant(), '60x60', 'fit']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.esport-city-tour.overview-ect-city.name')
            ->setRenderer(function (EctCity $ectCity): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-ect-city',
                        'id'      => $ectCity->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($ectCity->getName());

                return $name;
            });
        $this->addColumnIsActive($grid, 'esport-city-tour.overview');

        // STYLE
        $grid->getColumn('imagePreview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');
        $grid->getColumn('imagePreviewCity')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');
        $grid->getColumn('imagePreviewParticipant')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // FILTER
        $grid->addFilterText('name', 'grid.esport-city-tour.overview-ect-city.name', ['name']);
        $this->addFilterIsActive($grid, 'esport-city-tour.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.esport-city-tour.overview-ect-city.action.edit', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit-ect-city',
                ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.esport-city-tour.overview-ect-city.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-ect-city',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['name' => 'ASC']);

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.esport-city-tour.overview-ect-city.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
