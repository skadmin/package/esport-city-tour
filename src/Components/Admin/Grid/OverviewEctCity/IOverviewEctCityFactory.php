<?php

declare(strict_types=1);

namespace Skadmin\EsportCityTour\Components\Admin;

interface IOverviewEctCityFactory
{
    public function create(): OverviewEctCity;
}
