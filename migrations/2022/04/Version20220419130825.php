<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220419130825 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'esport-city-tour.overview-ect-city', 'hash' => '61e7b1b6cff600197fe531b1f2f1a819', 'module' => 'admin', 'language_id' => 1, 'singular' => 'ECT', 'plural1' => '', 'plural2' => ''],
            ['original' => 'esport-city-tour.overview-ect-city.title', 'hash' => '5b02a8fdb6c33100ff30445b59d9ebe3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Esport city tour - města|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.esport-city-tour.overview-ect-city.action.new', 'hash' => '89bf23c26f79ca93103c25a22a384728', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit nové město', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.esport-city-tour.overview-ect-city.image-preview', 'hash' => '3117b560730b3486ea9de90b57b82cab', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.esport-city-tour.overview-ect-city.image-preview-city', 'hash' => '5c392fd3e3da083305bbe7ff684d10ed', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Město', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.esport-city-tour.overview-ect-city.image-preview-participant', 'hash' => 'c0da704eebc43132a162664fbb6fcbca', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Účastník', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.esport-city-tour.overview-ect-city.name', 'hash' => 'b619656b21126b7b64dee66e3aaa2ff7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.esport-city-tour.overview.is-active', 'hash' => '0a2a554ad6036e2243492be5fe6d1df5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.esport-city-tour.overview-ect-city.action.edit', 'hash' => '0e0a8f73fbc81635adfe616dc9a558ef', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'esport-city-tour.edit-ect-city.title', 'hash' => 'fea59d78a5e0d8fd577cf492e0530038', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Esport city tour - založení města', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.name', 'hash' => '511a21e8ecae3dc4dc43e002de110b19', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.name.req', 'hash' => '5f8750fb2d66d310dc28af37283518e3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.is-active', 'hash' => 'a741f0441e9b54534af9f64c7eefe802', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.image-preview', 'hash' => 'a228e878c3fedf2c10c9407d2b8062c5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled města', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.image-preview.rule-image', 'hash' => '7899f8c40f980501d99d241108ee3947', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.image-preview-city', 'hash' => '4d49fb583156389d44128e7beb726431', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Logo města', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.image-preview-city.rule-image', 'hash' => '15c3567d8a06dbaec463dce0ab5ebfe9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.image-preview-participant', 'hash' => '7ae2b9e33933573a7814e831d8bd53e9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Logo účastníka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.image-preview-participant.rule-image', 'hash' => '9d8a2fc49181e55961b0bc924a9a78b0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.send', 'hash' => '66a02830ef3b9150df566292e2f12c10', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.send-back', 'hash' => 'c43cb914f7366948a33314d89c761987', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.back', 'hash' => '95c396f8db4c491dd2159bdfc8508330', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.flash.success.create', 'hash' => '00accd2b48b5e787f78b9d990158a756', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Esport city tour - město bylo úspěšně založeno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'esport-city-tour.edit-ect-city.title - %s', 'hash' => '4aa6ba477a73f2192018f14199e020d3', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace esport city tour - město', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.esport-city-tour.edit-ect-city.flash.success.update', 'hash' => '8acde54fddcb236ec644e71df443b492', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Esport city tour - město bylo úspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.esport-city-tour.overview-ect-city.flash.sort.success', 'hash' => '6bd9023ff60c33f2173d79c281f9ea2f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí esport city tour - město bylo upraveno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
